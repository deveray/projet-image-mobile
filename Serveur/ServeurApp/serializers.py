from rest_framework import serializers

from ServeurApp.models import Result


class TrackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = ['order', 'title', 'duration']


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Result
        fields = "__all__"

