from django.apps import AppConfig


class ServeurappConfig(AppConfig):
    name = 'ServeurApp'
