from keras.preprocessing import image
import Serveur.settings
from ServeurApp.extract_cnn_vgg16_keras import VGGNet
import numpy as np
import h5py

def image_recognition(image_path):
    h5f = h5py.File(Serveur.settings.CNN_DIR, 'r')
    feats = h5f['dataset_feat'][:]
    imgNames = h5f['dataset_name'][:]
    h5f.close()

    print("--------------------------------------------------")
    print("               searching starts")
    print("--------------------------------------------------")
    # init VGGNet16 model
    model = VGGNet()

    # extract query image's feature, compute simlarity score and sort
    queryVec = model.extract_feat(image_path)
    scores = np.dot(queryVec, feats.T)  # Liste de comparaison avec toutes les images de la db
    rank_ID = np.argsort(scores)[::-1]  # Sorting des meilleurs résultats
    rank_score = scores[rank_ID]

    # number of top retrieved images to show
    maxres = 6  # Nombres d'images ressemblant
    imlist = [imgNames[index] for i, index in enumerate(rank_ID[0:maxres])]
    #Initialisation du JSON
    list_result = {}
    for i in range(maxres):
        result = {'img_url': '/static/'+(imlist[i].decode()), 'score':str(rank_score[i])}
        list_result[i] = result
    return list_result

#Ce main est uniquement présent pour faire des tests
if __name__ == "__main__":
    input_shape = (224, 224, 3)
    img_path = "james_bond.png"
    img = image.load_img(img_path, target_size=(input_shape[0], input_shape[1]))
    listing_images = image_recognition(img)
    print(listing_images)