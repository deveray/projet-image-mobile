# Create your views here.

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
import json
from ServeurApp.models import Result
from .serializers import FileSerializer
from ServeurApp.query_online import image_recognition



class FileUploadView(APIView):
    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            # Calcul de la ressemblance avec le fichier H5
            listing_images = image_recognition(request.FILES["file"])
            # Generation du JSON
            my_json = json.dumps(listing_images)
            #Sauvegarde en BDD du JSON
            result = Result(image_urls=my_json)
            result.save()
            path_request_json = "http://192.168.0.35:8000/searchResult/" + str(result.pk)
            #Repondre 201 created avec dans le champ "Location" le lien vers le JSON correspondant à l'ID de l'image
            return Response(headers={"Location":path_request_json}, status=status.HTTP_201_CREATED)
        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    # Pour faire des requetes sur des images en statiques, il faut passer par l'URL complète de l'image  à partir de "Img"
    # Exemple : https://192.168.0.29:8000/static/MEN/Denim/id_000000080/01_1_front.jpg

class SearchResultView(APIView):
    def get(self, request,id):
        objet_bdd = get_object_or_404(Result,pk=id)
        my_json = objet_bdd.image_urls
        my_json = json.loads(my_json)
        return JsonResponse(my_json, safe=False)






