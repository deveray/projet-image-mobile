import h5py
import numpy as np
import os.path

from PIL import Image
from ServeurApp.extract_cnn_vgg16_keras import VGGNet

'''
 Extract features and index the images
'''

def ca_fait_tout(textfile):
    model = VGGNet()
    feats = []
    names = []
    with open(textfile) as f:
        # This permits to skip the first two lines which are unrelevant.
        lines_after_2 = f.readlines()[2:]

    for i,ligne in enumerate(lines_after_2)  :
        image_name, clothes_type, pose_type, x_1, y_1, x_2, y_2 = ligne.split()
        x_1,y_1,x_2,y_2 = map(int,[x_1,y_1,x_2,y_2])
        image_name = "database/" + image_name
        print("Current image : " + image_name)
        image = Image.open(image_name)
        cropped_image = (image.crop((x_1,y_1,x_2,y_2))).resize((224,224))
        traitement_feat_image(cropped_image,image_name,model,feats,names)
        if i == 100:
            break
    generateur_fichier_h5(feats,names)
def traitement_feat_image(image,image_name,model,feats,names):
    norm_feat = model.extract_feat_from_image(image)
    feats.append(norm_feat)
    names.append(image_name)



#This function creates the .h5 file with all the images characteristics
def generateur_fichier_h5(feats,names):
    feats = np.array(feats)
    output = 'featureCNN.h5'
    h5f = h5py.File(output, 'w')
    h5f.create_dataset('dataset_feat', data=feats)
    names = [name.encode("utf8") for name in names]
    h5f.create_dataset('dataset_name', data=names)
    h5f.close()

if __name__ == "__main__":
    path = os.path.abspath("database/Anno/list_bbox_inshop.txt")
    ca_fait_tout(path)
