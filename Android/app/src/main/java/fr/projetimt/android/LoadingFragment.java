package fr.projetimt.android;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class LoadingFragment extends DialogFragment {

    private TextView textLoading;

    private static final String MSG_EXTRA = "message";

    static LoadingFragment newInstance(String msg) {
        LoadingFragment fragment = new LoadingFragment();
        fragment.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putString(MSG_EXTRA, msg);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.view_loading, container, false);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        textLoading = view.findViewById(R.id.textLoading);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        textLoading.setText(getArguments().getString(MSG_EXTRA));
        this.setRetainInstance(true);
    }

    public void onUpdateMessage(String message) {
        textLoading.setText(message);
    }
}
