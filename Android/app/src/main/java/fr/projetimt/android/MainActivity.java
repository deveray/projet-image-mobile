package fr.projetimt.android;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.projetimt.android.volley.VolleyMultipartRequest;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_PHOTOGRAPHIER = 1;
    private static final int REQUEST_GALERIE = 2;

    private static final String SERVER_URL = "http://192.168.0.35:8000";

    protected ImageView preview;
    protected LoadingFragment loadingFragment;

    private Bitmap bitmap;
    private Bitmap requestedBitmap;
    private String currentPhotoPath;

    private ArrayList<Result> resultList;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultList = new ArrayList<>();

        preview = findViewById(R.id.preview);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (loadingFragment != null) {
            loadingFragment.dismiss();
        }
    }

    public void onPhotographierClick(View view) {
        dispatchTakePictureIntent();
    }

    public void onGalerieClick(View view) {
        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickIntent, REQUEST_GALERIE);
    }

    public void onEnvoyerClick(View view) {
        if (bitmap != null) {
            // Si l'image n'a pas change, on affiche directement le resultat
            if (bitmap.equals(requestedBitmap) && resultList != null) {
                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putParcelableArrayListExtra("resultList", resultList);
                startActivity(intent);
            } else {
                // Sinon on envoie l'image
                if (resultList != null) {
                    resultList.clear();
                }
                uploadBitmap(bitmap);
            }
        } else {
            AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Vous n'avez pas choisi d'image !");
            alertDialogFragment.show(getSupportFragmentManager(), "alert_dialog_fragment");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_PHOTOGRAPHIER) {
            if (resultCode == Activity.RESULT_OK) {
                File file = new File(currentPhotoPath);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(file));
                    if (bitmap != null) {
                        preview.setImageBitmap(bitmap);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == REQUEST_GALERIE) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    final Uri imageUri = data.getData();
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                    preview.setImageBitmap(bitmap);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Creation du fichier qui va contenir la photo
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "fr.projetimt.android.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_PHOTOGRAPHIER);
            }
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void uploadBitmap(final Bitmap bitmap) {

        loadingFragment = LoadingFragment.newInstance("Envoi en cours ...");
        loadingFragment.show(getSupportFragmentManager(), "loading_fragment");

        // Volley request pour envoyer l'image
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, SERVER_URL + "/upload/",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        loadingFragment.dismiss();
                        // On recupere l'url correspondant au resultat
                        String resultUrl = response.headers.get(LOCATION_SERVICE);

                        // On fait une requete get sur l'url pour recuperer le JSON
                        getJsonResult(resultUrl);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loadingFragment.dismiss();
                        AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Erreur lors de l'envoi !");
                        alertDialogFragment.show(getSupportFragmentManager(), "alert_dialog_fragment");
                    }
                }) {

            /*
             * On put l'image
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".jpg", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    private void getJsonResult(String resultUrl) {
        // On fait la requete GET sur l'url
        StringRequest stringRequest = new StringRequest(Request.Method.GET, resultUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject json = new JSONObject(response);

                            for (Iterator<String> it = json.keys(); it.hasNext(); ) {
                                String key = it.next();
                                // On recupere le resultat correspondant a la cle
                                JSONObject obj = (JSONObject) json.get(key);

                                // On ajoute le resultat dans la liste
                                Result result = new Result(SERVER_URL + obj.get("img_url"), (String) obj.get("score"));
                                resultList.add(result);
                            }

                            // On memorise le bitmap envoye
                            requestedBitmap = bitmap;

                            // On affiche un dialog pour confirmer l'affichage des resultats
                            AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Vous pouvez maintenant visualiser le résultat.");
                            alertDialogFragment.setPositiveButtonListener(new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                                    intent.putParcelableArrayListExtra("resultList", resultList);
                                    startActivity(intent);
                                }
                            });
                            alertDialogFragment.show(getSupportFragmentManager(), "alert_dialog_fragment");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialogFragment alertDialogFragment = AlertDialogFragment.newInstance("Erreur lors de la récupération du résultat !");
                alertDialogFragment.show(getSupportFragmentManager(), "alert_dialog_fragment");
            }
        });

        Volley.newRequestQueue(this).add(stringRequest);
    }
}
