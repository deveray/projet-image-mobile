package fr.projetimt.android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import fr.projetimt.android.volley.VolleySingleton;

public class ListViewAdapter extends ArrayAdapter<Result> {

    private Context context;

    public ListViewAdapter(Context context, int resourceId,
                           List<Result> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    private static class ViewHolder {
        NetworkImageView imageView;
        TextView scoreView;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        Result result = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_list, null);
            holder = new ViewHolder();
            holder.scoreView = convertView.findViewById(R.id.score);
            holder.imageView = convertView.findViewById(R.id.image);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        float percentageScore = Float.parseFloat(result.getScore()) * 100;
        holder.scoreView.setText(String.format("Score : %.2f %%", percentageScore));
        holder.imageView.setDefaultImageResId(android.R.drawable.stat_sys_download);
        holder.imageView.setImageUrl(result.getUrl(), VolleySingleton.getInstance(context).getImageLoader());

        return convertView;
    }
}