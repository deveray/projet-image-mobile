package fr.projetimt.android;

import android.os.Bundle;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        setTitle("Résultat de la recherche");

        ArrayList<Result> results = getIntent().getParcelableArrayListExtra("resultList");

        ListView listView = findViewById(R.id.list_view);
        ListViewAdapter adapter = new ListViewAdapter(this,
                R.layout.item_list, results);
        listView.setAdapter(adapter);
    }

}
