package fr.projetimt.android;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

public class AlertDialogFragment extends DialogFragment {

    private static final String MESSAGE_EXTRA = "MESSAGE";

    DialogInterface.OnClickListener positiveButtonListener;

    public static AlertDialogFragment newInstance(String message) {
        Bundle args = new Bundle();
        args.putString(MESSAGE_EXTRA, message);
        AlertDialogFragment f = new AlertDialogFragment();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.setRetainInstance(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString(MESSAGE_EXTRA);
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);
        alertDialog.setTitle("Message");
        alertDialog.setMessage(message);
        if (positiveButtonListener != null) {
            alertDialog.setPositiveButton("Ok", positiveButtonListener);
        } else {
            alertDialog.setPositiveButton("Ok", null);
        }
        return alertDialog.create();
    }

    public void setPositiveButtonListener(DialogInterface.OnClickListener listener) {
        this.positiveButtonListener = listener;
    }
}