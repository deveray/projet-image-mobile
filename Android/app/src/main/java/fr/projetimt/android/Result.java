package fr.projetimt.android;

import android.os.Parcel;
import android.os.Parcelable;

public class Result implements Parcelable {

    private String url;
    private String score;

    public Result(String url, String score) {
        this.url = url;
        this.score = score;
    }

    protected Result(Parcel in) {
        url = in.readString();
        score = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel in) {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeString(this.score);
    }
}
